﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TYPECINEMATIC { QUINEMATIC, MOVE, WAIT}

[System.Serializable]
public struct SCinematic
{
    public TYPECINEMATIC type;
    public float time;
    public GameObject tarjet;
    public string anim;
    public bool clicked;
}

public class Cinematics : MonoBehaviour {

    [SerializeField]
    GameObject[] objects;
    [SerializeField]
    SCinematic[] cinematics;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject[] getObjects()
    {
        return objects;
    }
}
