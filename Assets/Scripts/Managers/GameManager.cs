﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public enum TAGS { Untagged, Respawn, Finish, EditorOnly, Player }
//public enum SCENES { SPLASH, LOADING, MENU, GAME }
public enum SCENES { SPLASH, CODE2, MESA2, DESIGN2 }
public class GameManager : MonoBehaviour
{
    #region VARIABLES
    public static GameManager instance;
    private SCENES scene;
    private SCENES sceneToLoad;
    public bool paused = false;
    private const float MAXTIMER = 1.0f;
    //Delegates
    delegate void CurrentAction();
    CurrentAction currentAction;
    [SerializeField]
    private Canvas canvasPause;
    public GameObject player;
    public int defaultScene = 1;
    #endregion

    /*************************
             UNITY
    *************************/
    void Awake()
    {
        if (instance == null)
        {
            scene = SCENES.SPLASH;
            instance = this;
            DontDestroyOnLoad(gameObject);
            //StartCoroutine(timer(3));//Para cambiar de SPLASH A MENU
            currentAction += chexkIfPaused;//Con esto le damos la posibilidad de pause
            if (canvasPause != null)
                canvasPause.enabled = false;
            loadScene(defaultScene);
        }
        else
            Destroy(gameObject);
    }

    void Update()
    {
        if (currentAction != null)
            currentAction();
        if (player == null)
        {
            try
            {
                findPlayer();
            }
            catch (System.Exception)
            {
                Debug.LogWarning("Not player found");
            }
        }
    }

    /*************************
            FUNCTIONS
    *************************/

    #region PAUSE
    public void pause()
    {
        if (paused)//Si estoy en pause quito la pausa y activo el tiempo
        {
            currentAction -= showPaused;
            currentAction += unPaused;
        }
        else
        {
            currentAction += showPaused;
            Debug.Log("PAUSED");
        }
        currentAction -= pause;
    }

    public void showPaused()
    {
        //Mostrar canvas
        Time.timeScale = 0;
        paused = true;
        if (canvasPause != null)
            canvasPause.enabled = true;
    }

    public void unPaused()
    {
        Time.timeScale = 1;
        paused = false;
        currentAction -= unPaused;
        if (canvasPause != null)
            canvasPause.enabled = false;
    }

    public void chexkIfPaused()
    {
        if (Input.GetButtonDown("Pause"))
        {
            currentAction += pause;
        }
    }
    #endregion

    private void findPlayer()
    {
        player = GameObject.FindGameObjectWithTag(TAGS.Player.ToString());
    }

    /*************************
           LOADSCENES
    *************************/

    /// <summary> Cargar escena de una manera normal, usando el SceneManager.LoadScene</summary>
    /// <param name="sceneIndex"></param>
    public void loadScene(SCENES sceneIndex)
    {
        scene = sceneIndex;
        SceneManager.LoadScene((int)sceneIndex);
    }

    /// <summary>
    /// <para>Cargamos la escena Loading, mientras se carga la escena que enviemos a sceneIndex.</para>
    /// Guardamos la escena enviada que la pantalla de carga la vaya cargando.</summary>
    /// <param name="sceneIndex"></param>
    public void loadSceneLoading(SCENES sceneIndex)
    {
        /*
        sceneToLoad = sceneIndex;        //Guardo la Escene a cargar
        loadScene(SCENES.LOADING);      //LLamo a cargar al loading, este preguntara por que tiene que cargar con una coroutine
        //StartCoroutine(LoadSceneAs((int)index));
        */ //TODO Hay que crear la escena de carga =)
    }
    /// <summary>
    /// Carga asincrona la escena que mandemos.
    /// </summary>
    /// <param name="sceneIndex"></param>
    public void loadAsync(SCENES sceneIndex)
    {
        scene = sceneIndex;
        StartCoroutine(LoadSceneAs(sceneIndex));
    }
    /// <summary>
    /// Método para que sea llamado mientras estas cargando el loading.
    /// </summary>
    public void loading()
    {
        scene = sceneToLoad;
        StartCoroutine(LoadSceneAs(scene));
    }


    /*******************
        COROUTINES
    *******************/
    IEnumerator LoadSceneAs(SCENES sceneIndex)
    {
        AsyncOperation loading = SceneManager.LoadSceneAsync((int)sceneIndex);

        loading.allowSceneActivation = false; //Cuando termien la carga no cargue directamente.
        //Timer para la pantalla de carga
        float timer = 5.0f;
        while (loading.progress < 0.9f || timer >= 0)
        {
            //mostaríamos por pantalla
            timer -= Time.deltaTime;
            Debug.Log("Progress: " + loading.progress + ", Timer: " + timer);
            yield return null;
        }
        loading.allowSceneActivation = true;
        scene = sceneIndex;
    }

    IEnumerator timer(int time)
    {
        float t = 0;
        while (t < time)
        {
            t += Time.deltaTime;
            yield return null;
        }
        loadSceneLoading(SCENES.CODE2);//Antes era SCENES.MENU Esto está en desuso
    }

    void OnDestroy()
    {
        canvasPause = null;
        currentAction = null;
        instance = null;
    }

    /**************************
           GETS & SETS
    **************************/
    public SCENES getScene()            { return scene;         }
    public void setScene(SCENES scene)  { this.scene = scene;   }
    public SCENES getSceneToLoad()      { return sceneToLoad;   }
    public GameObject getPlayer()       {
        if (player == null)
            findPlayer();
        return player;        }
    public AAnimationSpineCharacter getPlayerAnim() { return player.GetComponentInChildren<AAnimationSpineCharacter>(); }
    public void loadScene(int sceneIndex)
    {
        if(paused)
            pause();
        loadScene((SCENES)sceneIndex);
    }
}

