﻿using UnityEngine;
using System.Collections;

public class ClickableObject : MonoBehaviour {
    
    protected bool justClicked = false;
    protected bool clicked = false;
    [SerializeField]
    protected bool mouseOver = false;
    private bool control = false;
    [SerializeField]
    protected bool comeHere = false;
    // protected GameObject player; //En el futuro lo cambiamo por Personaje ya que se meteran enemigos.

    void Start () {
	}
	
	void Update () {
        controlUpdate();
	}

    protected void controlUpdate()
    {
        if (control)//Cuando un objeto te dice que vayas a por el
        {
            control = false;  //Para saber si se ha pulsado
            justClicked = true; //Acaba de pulsar
            clicked = true;   //Si un objeto ha sido pulsado
            StartCoroutine(nextFrame());
            if (comeHere)
            {
                callPlayer();
            }
        }
    }

    protected virtual void callPlayer()
    {
        GameManager.instance.getPlayer().GetComponent<AMovement>().goToObjectPos(this.gameObject, transform.position);
    }

    IEnumerator nextFrame()
    {
        yield return 0;
        justClicked = false;
    }

    public void OnMouseUp()
    {
        GameObject _player = GameManager.instance.getPlayer();
        if (_player.GetComponent<AMovement>().isMovible())
            control = true;
    }

    public void OnMouseExit()
    {
        mouseOver = false;
    }
    public void OnMouseEnter()
    {
        mouseOver = true;
    }

    public bool isClicked()         {   return clicked;     }
    public void setClicked(bool click) { clicked = click;   }
    public bool isJustClicked()     {   return justClicked; }
}
