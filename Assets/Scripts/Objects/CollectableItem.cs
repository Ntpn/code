﻿using UnityEngine;
using UnityEditor;
using Spine.Unity;//Para SkeletonAnimation
using System.Collections;

[System.Serializable]
public struct CollectableAnimationNames
{
    public string playerWalk;
    public string playerStop;
    public string playerGetItem;
    public string playerDrop;
    [SerializeField]
    [HideInInspector]
    private string _dropItemAnim;
    [SerializeField]
    [HideInInspector]
    private string _idleItemAnim;
    [SerializeField]
    [HideInInspector]
    private string _hideItemAnim;

    public string dropItemAnim
    {
        get
        {
            return _dropItemAnim;
        }

        set
        {
            _dropItemAnim = value;
        }
    }
    public string idleItemAnim
    {
        get
        {
            return _idleItemAnim;
        }

        set
        {
            _idleItemAnim = value;
        }
    }
    public string hideItemAnim
    {
        get
        {
            return _hideItemAnim;
        }

        set
        {
            _hideItemAnim = value;
        }
    }
}

[System.Serializable]
public struct ActiveCondition
{
    public GameObject prefab;
    [HideInInspector]
    public string playerAnim;
}

public enum States
{
    IDLE, WALK, GET, DROP
}

[RequireComponent (typeof(BoxCollider2D))]
[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(SkeletonAnimation))]
public class CollectableItem : ClickableObject {
    [SerializeField]
    private Vector3 localLeft;
    [SerializeField]
    private Vector3 localRight;
    [SerializeField]
    protected CollectableAnimationNames animationNames;
    private SkeletonAnimation _skeletonAnimation;
    protected SkeletonAnimation skeletonAnimation
    {
        get { return _skeletonAnimation; }
        set { _skeletonAnimation = value; }
    }
    private bool _control = true;
    //[SerializeField]
    protected Vector2 _desplazament = Vector2.zero;
    [SerializeField]
    protected ActiveCondition activators;
    private GameObject otherCombinable;
    private Combinable myCombinable;
    [HideInInspector]
    public States state;


    // Use this for initialization
    void Start()
    {
        //TODO Lo estoy metiendo a mano, quizas esto lo deba cambiar desde el inspector =).
        
        animationNames.idleItemAnim = animationNames.idleItemAnim != null ? animationNames.idleItemAnim : "idle";
        animationNames.hideItemAnim = animationNames.hideItemAnim != null ? animationNames.hideItemAnim : "coger";
        animationNames.dropItemAnim = animationNames.dropItemAnim != null ? animationNames.dropItemAnim : "soltar";
        gameObject.tag = "ObjectUsable";
        gameObject.layer = LayerMask.NameToLayer("ObjectUsable");
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        _desplazament = new Vector2(skeletonAnimation.Skeleton.RootBone.x, skeletonAnimation.Skeleton.RootBone.y);
        //root = GetComponentInChildren<SkeletonUtilityBone>().transform;

        skeletonAnimation.state.End += delegate (Spine.AnimationState state, int trackIndex) {
            //Cuando acabe la animacion si es en el track 0 y no tiene loop (una accion) la ponemos en idle
            if (trackIndex==0 && !state.GetCurrent(trackIndex).loop)
            {
                //El control lo hacemos porque al cambiar de animación se llama al delegado END y como lo estoy llamando dentro
                //del propio delegado se queda en bucle infinito así que me creo el bool de control para saber si lo he llamado desde dentro
                if (_control)
                {
                    _control = false;
                    StartCoroutine("UpdatePosition");
                    //changeAnim(findAnimationByName(animationNames.idleItemAnim), true);
                    _control = true;
                }
            }
        };
        skeletonAnimation.state.Event += HandleEvent;

        myCombinable = GetComponent<Combinable>() ?? null;//Campturamos si se combina con algo
        otherCombinable = myCombinable != null ? myCombinable.getOther() : null; //Si tiene combinable, capturo quien es en other

        //Pongo en idle.
        changeAnim(findAnimationByName(animationNames.idleItemAnim),true);
    }

    private void HandleEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
        //Control de eventos en las animaciones de los objetos
        /*
        if (e.Data.Name.CompareTo("AnimationEnd") == 0)
        {

        }*/
    }

    private IEnumerator UpdatePosition()
    {
        yield return new WaitForEndOfFrame();
        changeAnim(findAnimationByName(animationNames.idleItemAnim), true);
        Vector2 t = new Vector2(skeletonAnimation.skeleton.flipX ? -skeletonAnimation.Skeleton.RootBone.x + _desplazament.x : skeletonAnimation.Skeleton.RootBone.x - _desplazament.x, skeletonAnimation.Skeleton.RootBone.y - _desplazament.y);
        transform.Translate(t);
    }

    private void State_Start(Spine.AnimationState state, int trackIndex)
    {
        throw new System.NotImplementedException();
    }

    protected void Update()
    {
        controlUpdate();
    }

    protected override void callPlayer()
    {
        Vector3 playerPos = GameManager.instance.getPlayer().transform.position;
        Vector3 pointPosition = transform.position;
        if (playerPos.x < transform.position.x + localLeft.x)
            pointPosition = transform.position + localLeft;
        else if (playerPos.x > transform.position.x + localRight.x)
        {
            pointPosition = transform.position + localRight;
        }
        else
        {
            if ((playerPos.x + localRight.x/2)< localRight.x )
                pointPosition = transform.position + localRight;
            else
                pointPosition = transform.position + localLeft;
        }
        GameManager.instance.getPlayer().GetComponent<AMovement>().goToObjectPos(this.gameObject, pointPosition);
    }

    public void action()
    {
        //Asignamos las animaciones y luego cogemos el objeto
        GameObject player = GameManager.instance.getPlayer();
        if (!player.GetComponentInChildren<AnimSpineCharacter>().gotItem())//Si no tienes objetos
        {
            if (activators.prefab == null)//No se activa con un objeto
            {
                //Lo cogemos
                setAnimPlayer(animationNames.playerStop, animationNames.playerDrop, animationNames.playerGetItem, animationNames.playerWalk);
                player.GetComponentInChildren<AnimSpineCharacter>().getItem(gameObject);
            }
            else
            {
                //Solo se puede coger con el activador
                player.GetComponentInChildren<AnimSpineCharacter>().cantGetItem();
            }

        }else
        {//Tiene cogido un objeto
            GameObject item = player.GetComponentInChildren<AnimSpineCharacter>().item();
            if (activators.prefab == null)//ACTIVADOR == NULL
            {
                //Si tengo que combinarme Y el objeto que lleva el player es con quien me combino
                if (myCombinable != null && otherCombinable == item)
                {
                    myCombinable.combine();
                }
                else
                    player.GetComponentInChildren<AnimSpineCharacter>().cantGetItem();
            }else
            {
                //TODO AQUI DEBERIAMOS DECIRLE QUE SE HAGA UNA ANIMACION DE INTERACTUAR Y EN LA MISMA SOLTARÍA EL OTRO OBJETO
                //Es el objeto que lo activa
                if (item == activators.prefab)
                {
                    //Liberamos el objetoS
					player.GetComponentInChildren<AnimSpineCharacter>().addAction(activators.playerAnim,true);
                    liberate();
                }
                else  //Tiene objeto y activador, pero no es el que tenemos cogido
                {
                    player.GetComponentInChildren<AnimSpineCharacter>().cantGetItem();
                }

            }
        }

    }
    public void hide()
    {
        //No podemos hacer setActive da problemas con Spine, asi que le digo que no tenga fisica =)
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().Sleep();
        GetComponent<MeshRenderer>().enabled = false;
    }
    private void show()
    {
        GetComponent<BoxCollider2D>().enabled = true;
        GetComponent<Rigidbody2D>().WakeUp();
        GetComponent<MeshRenderer>().enabled = true;

    }

    public void get()
    {
        changeAnim(findAnimationByName(animationNames.hideItemAnim),true);
        hide();
    }

    public void drop(bool flix)
    {
        show();
        skeletonAnimation.skeleton.flipX = flix;
        changeAnim(findAnimationByName(animationNames.dropItemAnim),false);
    }

    protected void liberate()
    {
        changeAnim(findAnimationByName(animationNames.dropItemAnim), false);
        activators.prefab = null;
        activators.playerAnim = "";
    }

    protected void changeAnim(Spine.Animation anim, bool loop)
    {
        skeletonAnimation.state.SetAnimation(0, anim, loop);
    }

    //Buscar animacion
    public Spine.Animation findAnimationByName(string nameAnim)
    {
        foreach (Spine.Animation _anim in skeletonAnimation.SkeletonDataAsset.GetSkeletonData(false).animations)
            if (_anim.name.CompareTo(nameAnim) == 0)
                return _anim;
        return null;
    }

    public void setAnimPlayer(string playerStop, string playerDrop, string playerGetItem, string playerWalk)
    {
        GameManager.instance.getPlayerAnim().setObjectAnimNames(playerStop, playerDrop, playerGetItem, playerWalk);
    }

    //GET && SET
    public ActiveCondition getActivators()
    {
        return activators;
    }

    public void setActivators(string name)
    {
        activators.playerAnim = name;
    }

    public CollectableAnimationNames getCollectAnimNames()
    {
        return animationNames;
    }
    public string getIdle() { return animationNames.idleItemAnim; }
    public void setIdle(string idleNameAnim)
    {
        animationNames.idleItemAnim = idleNameAnim;
    }
    public string getHide() { return animationNames.hideItemAnim; }
    public void setHide(string hideNameAnim)
    {
        animationNames.hideItemAnim = hideNameAnim;
    }
    public string getDrop() { return animationNames.dropItemAnim; }
    public void setDrop(string dropNameAnim)
    {
        animationNames.dropItemAnim = dropNameAnim;
    }

    public GameObject getOtherCombinable()
    {
        return otherCombinable;
    }

}
