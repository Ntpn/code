﻿using UnityEngine;
using Spine.Unity;

[RequireComponent (typeof(SkeletonAnimation))]
public class AnimationSpineObject : MonoBehaviour {
    [SerializeField] private string animName;

    public void startAnim(bool flipX)
    {
        GetComponentInChildren<SkeletonAnimation>().skeleton.SetToSetupPose();
        GetComponentInChildren<SkeletonAnimation>().skeleton.flipX = flipX;
        GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0, animName, false);
    }
}
