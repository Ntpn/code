﻿using UnityEngine;
using System.Collections;
//public enum TypeCombine { STANDARD }

[RequireComponent (typeof(CollectableItem))]
public class Combinable : MonoBehaviour {
    [SerializeField]
    private GameObject other;
    private CollectableItem myCollectable;
    [SerializeField]
    private string animCombine;
    [SerializeField]
    private GameObject newPrefab;

    public void Start()
    {
        myCollectable = GetComponent<CollectableItem>();
    }

    public void combine()
    {

        GameObject pGo = GameManager.instance.getPlayer();
        AnimSpineCharacter animSpineCharacter = pGo.GetComponentInChildren<AnimSpineCharacter>();
        animSpineCharacter.addAction(animCombine);
        animSpineCharacter.cantMove();

    }

    public bool correctItem(Combinable _other)
    {
        return _other == other;
    }

    public GameObject getOther()
    {
        return other;
    }

    public GameObject createPrefab()
    {
        GameObject go = Instantiate(newPrefab);
        CollectableItem col = go.GetComponent<CollectableItem>();
        GameObject _pla = GameObject.FindGameObjectWithTag("Player");
        AnimSpineCharacter _plaAnim = _pla.GetComponentInChildren<AnimSpineCharacter>();
        _plaAnim.setCollectableAnimNames(col.getCollectAnimNames());
        col.hide();
        
        //Una vez hecho se destruyen ambos y se crea el nuevo.

        StartCoroutine(destroyCombinables());
        return (go);
    }

    private IEnumerator destroyCombinables()
    {
        yield return new WaitForEndOfFrame();
        Destroy(other);
        Destroy(gameObject);
    }

}
