﻿using UnityEngine;
using System;//para el Array

/************
 * OBSOLETO *
 ************/

[RequireComponent (typeof(AAnimationSpineCharacter))]
public class GUICharacterSpine : MonoBehaviour {
    //Para controlar las animaciones de Spine
    AAnimationSpineCharacter animationSpineCharacter;
    public Spine.Animation[] animaciones;

    //GUI
    //Tipo
    private int gridAnt = 0;
    private int gridSelec = 0;
    public string[] gridTipo = new string[] { "General", "Fase 01", "Fase 02", "Fase 03", "Fase 04", "Fase 05", "Fase 06", "Otros" };
    #region ANIMACIONES
    //Animaciones
    private int selGridInt = 0;
    private int anteriorGrid = 0;
    private string[] selStrings = new string[0];
    private string[] selGeneral = new string[0];
    private string[] sel01 = new string[0];
    private string[] sel02 = new string[0];
    private string[] sel03 = new string[0];
    private string[] sel04 = new string[0];
    private string[] sel05 = new string[0];
    private string[] sel06 = new string[0];
    private string[] selOtros = new string[0];
    #endregion
    //Buttons
    private bool isMovible = false;

    //Hueso Texto
    private TextMesh textoHueso;

    void Awake()
    {
        animationSpineCharacter = GetComponent<AAnimationSpineCharacter>();
        GameObject objText = new GameObject();
        objText.name = "Character Text Position";
        textoHueso = objText.AddComponent<TextMesh>();
        textoHueso.fontSize = 12;
        textoHueso.characterSize = 0.2f;
    }

    void Start () {
        //No podemos hacerlo en el awake, ya que no estarán las animaciones y da problemas.
        #region CARGAR ARRAY POR FASES

        animaciones = animationSpineCharacter.skeletonAnimation.SkeletonDataAsset.GetSkeletonData(false).animations.Items; //Todas las animaciones
        selStrings = new string[animaciones.Length];//Temporalmente creo un array de String

        foreach (Spine.Animation anima in animaciones)
        {
            if (anima.name.StartsWith("#"))//Dependiendo del nombre # General, 01 Fase1 -> 06 Fase6, y si no en Otros
            {
                Array.Resize<String>(ref selGeneral, selGeneral.Length + 1);
                selGeneral[selGeneral.Length - 1] = anima.name;
            }
            else if (anima.name.StartsWith("01"))
            {
                Array.Resize<String>(ref sel01, sel01.Length + 1);
                sel01[sel01.Length - 1] = anima.name;
            }
            else if (anima.name.StartsWith("02"))
            {
                Array.Resize<String>(ref sel02, sel02.Length + 1);
                sel02[sel02.Length - 1] = anima.name;
            }
            else if (anima.name.StartsWith("03"))
            {
                Array.Resize<String>(ref sel03, sel03.Length + 1);
                sel03[sel03.Length - 1] = anima.name;
            }
            else if (anima.name.StartsWith("04"))
            {
                Array.Resize<String>(ref sel04, sel04.Length + 1);
                sel04[sel04.Length - 1] = anima.name;
            }
            else if (anima.name.StartsWith("05"))
            {
                Array.Resize<String>(ref sel05, sel05.Length + 1);
                sel05[sel05.Length - 1] = anima.name;
            }
            else if (anima.name.StartsWith("06"))
            {
                Array.Resize<String>(ref sel06, sel06.Length + 1);
                sel06[sel06.Length - 1] = anima.name;
            }
            else //Otros
            {
                Array.Resize<String>(ref selOtros, selOtros.Length + 1);
                selOtros[selOtros.Length - 1] = anima.name;
            }
        }
        //Inicializo el grid
        selStrings = selGeneral;
        #endregion
    }

    void Update()
    {
        //Recalculo la posicion del texto
        textoHueso.transform.position = transform.position;
        textoHueso.text = "x: " + animationSpineCharacter.skeletonAnimation.skeleton.RootBone.x + "\n y: " + animationSpineCharacter.skeletonAnimation.skeleton.RootBone.y;
    }

    void OnGUI()
    {
#if UNITY_EDITOR
        //Primer RadioButon
        gridSelec = GUILayout.SelectionGrid(gridSelec, gridTipo, gridTipo.Length, UnityEditor.EditorStyles.radioButton);
        if (gridSelec != gridAnt)
        {
            gridAnt = gridSelec;
#region Switch Grid
            switch (gridSelec)
            {
                case 0://General
                    selStrings = selGeneral;
                    break;
                case 1://Fase 01
                    selStrings = sel01;
                    break;
                case 2://Fase 02
                    selStrings = sel02;
                    break;
                case 3://Fase 03
                    selStrings = sel03;
                    break;
                case 4://Fase 04
                    selStrings = sel04;
                    break;
                case 5://Fase 05
                    selStrings = sel05;
                    break;
                case 6://Fase 06
                    selStrings = sel06;
                    break;
                case 7://Otros
                    selStrings = selOtros;
                    break;
                default:
                    break;
            }
#endregion
        }
        //Buttons
        if (GUI.Button(new Rect(25, 25, 50, 24), "Reset"))
        {
            transform.parent.position = new Vector3(5.86f, 0, 0);
        }
        if (GUI.Button(new Rect(75.5f, 25, 85.5f, 24), isMovible ? "Quitar mov" : "Poner mov"))
        {
            isMovible = !isMovible;
            animationSpineCharacter.setMovible(isMovible);
        }
        //Anim grid
        selGridInt = GUI.SelectionGrid(new Rect(25, 50, 600, 450), selGridInt, selStrings, 5);
        if (anteriorGrid != selGridInt)
        {
            anteriorGrid = selGridInt;
            try
            { cambiarAnimacio(buscarAnim(selStrings[selGridInt])); }
            catch (System.Exception)
            { Debug.Log("No encontrada la animacion"); }
        }
#endif
    }


    private void cambiarAnimacio(Spine.Animation anim)
    {
        try { animationSpineCharacter.changeAnimation(anim); }
        catch (System.Exception e) { Debug.Log("Error al hacer la animacion. " + e.ToString()); }
    }

    public Spine.Animation buscarAnim(string nameAnim)
    {
        foreach (Spine.Animation _anim in animaciones)
            if (_anim.name.CompareTo(nameAnim) == 0)
                return _anim;
        return null;
    }

    //Método para controlar los eventos
    void HandleEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
        Debug.Log(e.Data.Name);
    }

    private void OnDestroy()
    {
        animaciones =   null;
        gridTipo =      null;
        selStrings =    null;
        selGeneral =    null;
        sel01 =         null;
        sel02 =         null;
        sel03 =         null;
        sel04 =         null;
        sel05 =         null;
        sel06 =         null;
        selOtros =      null;
        textoHueso =    null;
    }
}
