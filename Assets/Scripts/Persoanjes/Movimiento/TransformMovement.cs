﻿using System;
using UnityEngine;

public class TransformMovement : AMovement {
    public override void move()
    {
        if (lookRight)
        {
            //translate(transform.forward * speed);
            translate(new Vector2(1, 0) * speed * Time.deltaTime);
        }
        else
        {
            translate(-new Vector2(1, 0) * speed * Time.deltaTime);
        }
    }

    public override void stop()
    {
        Debug.Log("TransformMovement: Stop, no implementado!!!");
    }

    public override void translate(Vector2 desplacement)
    {
        transform.Translate(desplacement);
    }
}
