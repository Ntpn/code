﻿using UnityEngine;

public interface IMovible {

    void move();
    void translate(Vector2 desplacement);
}
