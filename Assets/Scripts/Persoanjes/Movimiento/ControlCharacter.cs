﻿using UnityEngine;

[RequireComponent (typeof(AMovement))]
public class ControlCharacter : MonoBehaviour {
    AMovement movement;

	void Start () {
        movement = GetComponent<AMovement>();
	}
	
	// Update is called once per frame
	void Update () {
        //TODO Habra que modificarlo por getButton
        if (movement.isMovible())
        {
            if (Input.GetKey(KeyCode.A))
            {
                movement.controlLeft();
            }
            else if (Input.GetKey(KeyCode.D))
            {
                movement.controlRight();
            }
            else
            {
                movement.noMoveControl();
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                if (GetComponentInChildren<AnimSpineCharacter>().gotItem())
                    movement.action();
            }
        }
        
    }

    void OnDestroy()
    {
        movement = null;
    }
}
