﻿using System;
using UnityEngine;

public abstract class AMovement: MonoBehaviour, IMovible
{
    //Atributos
    [SerializeField]
    protected float speed=1;
    protected bool lookRight=true;
    [SerializeField]
    protected AAnimationSpineCharacter spineAnim;
    [SerializeField]
    protected bool movible = true;
    #region GOTOPOS
    private Vector2 positionToGo = Vector2.zero;
    [SerializeField]
    private bool movingToPos = false;
    [SerializeField]
    private GameObject objectToGo;
    #endregion
    /*********************************************
     *                  UNITY                    *
     ********************************************/
    void Update()
    {
        if (movingToPos && objectToGo != null)//Si un objeto externo le dice que se acerque
        {
            float len = 0;
            if (this.transform.position.x < positionToGo.x)//El objeto está a la izquierda
            {
                moveRight();
                len = positionToGo.x - transform.position.x;
            }
            else
            {
                moveLeft();
                len = transform.position.x - positionToGo.x;

            }

            /*
             //TEMPORALMENTE ESTOY USANDO LA DISTANCIA SOBRE LA X. MÁS ADELANTE USARMOS EL SQRMAGNITUDE DE AQUI ABAJO *
            float len = (transform.position - objectToGo.transform.position).sqrMagnitude;
            Debug.Log(len);*/

            if (len < 0.05)//Calcular si está suficientemente cerca
            {
                movingToPos = false;
                transform.position = new Vector3(positionToGo.x, transform.position.y, transform.position.z); //new Vector3(objectToGo.transform.position.x, transform.position.y, transform.position.z);//Lo pongo en el sitio exacto
                //Si estaba cerca del objeto y me he movido a la izquierda para llegar a la posicion correcta
                if (transform.position.x < objectToGo.transform.position.x && !lookRight)
                    switchLook();
                else if (transform.position.x > objectToGo.transform.position.x && lookRight)
                    switchLook();
                //TODO interactuar con el objeto
                actionObject();
                clearObjectPos();//Ya hemos llegado al objeto
            }
        }
    }
    /*********************************************
     *                FUNCTIONS                  *
     ********************************************/
    public void controlLeft()
    {
        if (movingToPos)
        {
            movingToPos = false;
            objectToGo.GetComponent<ClickableObject>().setClicked(false);
        }
        movingToPos = false;
        moveLeft();
    }
    public void controlRight()
    {
        if (movingToPos)
        {
            movingToPos = false;
            objectToGo.GetComponent<ClickableObject>().setClicked(false);
        }
        movingToPos = false;
        moveRight();
    }

    public void moveLeft()
    {
        if (lookRight)
            switchLook();
        move();
    }

    public void moveRight()
    {
        if (!lookRight)
            switchLook();
        move();
    }

    public void goToObjectPos(GameObject objectToGo, Vector2 position)
    {
        this.objectToGo = objectToGo;
        movingToPos = true;
        positionToGo = position;
    }

    public void goToPos(Vector2 position)
    {
        movingToPos = true;
        positionToGo = position;
    }

    private void clearObjectPos()
    {
        if (objectToGo != null)
        {
            objectToGo.GetComponent<ClickableObject>().setClicked(false);
            objectToGo = null;
            movingToPos = false;
        }
    }

    private void actionObject()
    {
        if (objectToGo.GetComponent<ClickableObject>() is CollectableItem)
        {
            objectToGo.GetComponent<CollectableItem>().action();
        }
        else
        {
            clearObjectPos();
        }
    }

    public void noMoveControl()
    {
        if (!movingToPos)
        {
            stop();
        }
    }

    //Giro
    private void flip()
    {
        try {
            gameObject.GetComponentInChildren<AAnimationSpineCharacter>().flip();
        } catch (System.Exception e) { Debug.Log(e);
        }
    }

    public void action()
    {
        clearObjectPos();
        spineAnim.actionDrop();
    }

    public void switchLook()
    {
        flip();
        lookRight = !lookRight;
    }

    /*********************************************
     *            GETTERS & SETTERS              *
     ********************************************/
    public AAnimationSpineCharacter getSpineAnim()  { return spineAnim;         }
    public bool isMovible()                         { return movible;           }
    public void setMovible(bool isMovible)          { movible = isMovible;      }
    public void setMovingToPos(bool movingToPos)    { this.movingToPos = movingToPos; }
    public bool isLookRight()                       { return this.lookRight;    }
    public void setLookRight(bool lookingRight)
    {
        //Cambiar de lado
        if (lookRight != lookingRight)
        {
            flip();
        }
        lookRight = lookingRight;
    }

    //Destructor
    void OnDestroy()
    {
        spineAnim = null;
    }

    //Abstracto - Este lo modificamos segun transform o rigidbody
    public abstract void translate(Vector2 desplacement);
    public abstract void stop();
    public abstract void move();
}
