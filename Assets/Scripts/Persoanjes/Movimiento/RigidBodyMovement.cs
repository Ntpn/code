﻿using System;
using UnityEngine;

[RequireComponent (typeof(Rigidbody2D))]
public class RigidBodyMovement : AMovement
{
    protected Rigidbody2D rigidbody;

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    public override void translate(Vector2 velocity)
    {
        rigidbody.velocity = velocity;
    }

    void OnDestroy()
    {
        rigidbody = null;
    }

    public override void stop()
    {
        rigidbody.velocity = Vector2.zero;
        spineAnim.stop();
    }

    public override void move()
    {
        if (lookRight)
        {
            //translate(transform.forward * speed);
            translate(new Vector2(1, 0) * speed * Time.deltaTime);
        }
        else
        {
            translate(-new Vector2(1, 0) * speed * Time.deltaTime);
        }
        spineAnim.move();
    }
}
