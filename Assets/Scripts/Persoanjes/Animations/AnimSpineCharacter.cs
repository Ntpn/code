﻿using UnityEngine;
using System.Collections;

public class AnimSpineCharacter : AAnimationSpineCharacter {

    [SerializeField]
    private GameObject _item;
    [SerializeField]
    private GameObject _interactiveItem;
    [SerializeField]
    private Transform root;
    protected override void handleEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
        //Debug.Log(e.Data.Name);//Aqui podremos controlar nuestros eventos de la animacion
        if (e.Data.Name.CompareTo("SoltarObjeto") == 0)
        {
            if (_item != null)
                dropItem();
            else
                Debug.LogWarning("Se ha lanzado el evento de soltar objeto y no se tiene ninguno!!!!");
        }
        else if (e.Data.Name.CompareTo("CogerObjeto") == 0)
        {
            if (_item != null)
                _item.GetComponent<CollectableItem>().get();
            else
                Debug.LogWarning("Se ha lanzado el evento de coger objeto y no se tiene ninguno!!!!");
        }
        else if (e.Data.Name.CompareTo("CombinarObjeto") == 0)
        {
            try
            {
                Combinable comb = _item.GetComponent<Combinable>();
                _item = comb.createPrefab();
            }
            catch (System.Exception)
            {
                Debug.LogWarning("Se ha lanzado el evento de combinar pero ha habido errores.");
            }
        }
    }

    protected override void init(out string walk, out string walkWithItem, out string stoped, out string stopedWithItem, out string get, out string drop)
    {
        walk = WALKANIM;
        walkWithItem = "";
        stoped = STOPANIM;
        stopedWithItem = "";
        get = "";
        drop = "";
    }
    private void initAnim()
    {
        //Posicion inicial (Parado sin objetos)
        animNames.stoped = STOPANIM;
        animNames.walk = WALKANIM;
        animNames.get = "";
        animNames.drop = "";
        animNames.stopedWithItem = "";
        animNames.walkWithItem = "";
        usingItems = false;
    }

    public void getItem(GameObject collectionableItem)
    {
        if (actionGet())//Si ha ido bien la animacion
        {
            //no dejamos que se mueva mientras dura la animación
            movement.setMovible(false);
            //Asiganmos el objeto, le decimos que no tenía nada para que el solo lo coja y hacemos la accion de coger
            _item = collectionableItem;
        }
    }

    public void dropItem()
    {
        //no dejamos que se mueva mientras dura la animación
        movement.setMovible(false);
        _item.transform.position = root.position;
        _item.GetComponent<CollectableItem>().drop(skeletonAnimation.skeleton.flipX);
        _item = null;
        initAnim();
    }


    public bool gotItem()
    {
        bool r = false;
        if (_item != null)
            r = true;
        return r;
    }

    public GameObject item()
    {
        return _item;
    }

    public void cantGetItem()
    {
        movement.setMovible(false);
        stop();//Pongo la animacion en parado
        no();
    }

    protected override void no()
    {
        addEvent("#no");
    }

    public void cantMove()
    {
        movement.setMovible(false);
    }
    public void canMove()
    {
        movement.setMovible(true);
    }

}
