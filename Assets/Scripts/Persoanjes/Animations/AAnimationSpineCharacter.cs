﻿using UnityEngine;

using Spine.Unity;//Para SkeletonAnimation
using System.Collections;

/********************************************************************
 * Método intermedio entre Spine y nuestra clase personaje          *
 * en ella podemos añadir eventos y controlar cambos de animaciones *
 *******************************************************************/
[System.Serializable]
public struct AnimationsName
{
    public string walk;
    public string walkWithItem;
    public string stoped;
    public string stopedWithItem;
    public string action;
    public string get;
    public string drop;
}


public enum ANIMSTATE { START, COMPLETE, UPDATE,END}
[RequireComponent(typeof(SkeletonAnimation))]
public abstract class AAnimationSpineCharacter : MonoBehaviour {
    protected string STOPANIM = "#parado";
    protected string WALKANIM = "#andar";
    [SerializeField]
    protected bool usingItems = false;

    [SerializeField]
    protected AMovement movement;
    private SkeletonAnimation _skeletonAnimation;
    public SkeletonAnimation skeletonAnimation
    {   get { return _skeletonAnimation;    }
        set { _skeletonAnimation = value;   }   }

    private Vector2 posInice;
    private Vector2 desplacement;
    private bool isMovible;
    private ANIMSTATE state=ANIMSTATE.START;
    private string nameAnimUsing = "";
    [SerializeField]
    protected AnimationsName animNames;
    protected bool refreshSetToPose = false;
    protected bool blockedAnim = false;
    
    void Awake () {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        string walk, walkWithItem, stoped, stopedWithItem, get, drop;
        init(out walk, out walkWithItem, out stoped, out stopedWithItem, out get, out drop);
        animNames.walk =            walk;
        animNames.walkWithItem =    walkWithItem;
        animNames.stoped =          stoped;
        animNames.stopedWithItem =  stopedWithItem;
        animNames.get =             get;
        animNames.drop =            drop;

    }

    protected abstract void init(out string walk, out string walkWithItem, out string stoped, out string stopedWithItem, out string get, out string drop);

    void Start()
    {
        /****************************************************************
         ******************** CONTROLADOR DE EVENTOS ********************
         ****************************************************************
         * El ciclo de vida de los eventos es:                          *
         *              START -> COMPLETE -> END                        *
         * Si tiene loop la animacion se repite COMPLETE por cada loop  *
         *       START -> COMPLETE -> COMPLETE -> COMPLETE -> END       *
         * Si cancelamos una animacion quedaria                         *
         *                  START -> END                                *
         ***************************************************************/

        skeletonAnimation.state.Event += HandleEvent;//Para controlar los eventos
        
        skeletonAnimation.state.Start += delegate {
            //si necesitamos refrescar la posicion debido a los objetos que cogemos/soltamos
            //OBSOLETE SOLO SI TENEMOS VARIOS MATERIALES
            if (refreshSetToPose)
            {
                skeletonAnimation.skeleton.SetToSetupPose();
                refreshSetToPose = false;
            }
        };
        skeletonAnimation.state.Start += delegate (Spine.AnimationState state, int trackIndex) {
            posInice = Vector2.zero;
            this.state = ANIMSTATE.START;
            StartCoroutine("NextFrame");
        };
        

        skeletonAnimation.state.Complete += delegate (Spine.AnimationState state, int trackIndex, int loopCount) {
            //Si es el primer loop controlo el desplazamiento de la animacion
            if (loopCount == 1)
                desplacement = new Vector2(skeletonAnimation.Skeleton.RootBone.x, skeletonAnimation.Skeleton.RootBone.y) - posInice;

            if (isMovible)
            {
                transform.parent.Translate(new Vector2(skeletonAnimation.skeleton.FlipX ? -desplacement.x : desplacement.x, desplacement.y));
            }
            this.state = ANIMSTATE.COMPLETE;
            //Por si hay algún objeto que no debe estár en la animación.
            skeletonAnimation.skeleton.SetToSetupPose();

        };
        skeletonAnimation.state.End += delegate (Spine.AnimationState state, int trackIndex) {
            //Si queremos controlar cuando acaba
            this.state = ANIMSTATE.END;
            if (!state.GetCurrent(trackIndex).loop)//Si ha terminado una accion/evento la pongo en estado quieto
            {
                if (usingItems)
                    changeAnimation(animNames.stopedWithItem, true);
                else
                    changeAnimation(animNames.stoped, true);
                movement.setMovible(true);
                blockedAnim = false; //digo que ya ha terminado la animacion y puedo volver a poner una animacion
            }
        };
    }

    public bool isComplete()
    {
        return state == ANIMSTATE.COMPLETE;
    }

    //Para controlar los estados de las animaciones
    private IEnumerator NextFrame()
    {
        yield return new WaitForEndOfFrame();
        state = ANIMSTATE.UPDATE;
    }

    //Evento
    private void HandleEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
        handleEvent(state, trackIndex, e);
    }


    protected abstract void handleEvent(Spine.AnimationState state, int trackIndex, Spine.Event e);

    protected abstract void no();

    //Metodos para cambiar animaciones
    public void changeAnimation(Spine.Animation anim)
    {
        changeAnimation(anim, true);
    }

    public bool changeAnimation(Spine.Animation anim, bool loop)
    {
        try
        {
            //Si la animacion no es nula, no es la que estoy usando y no tenemos blokeada la animacion.
            if (anim != null && anim.name.CompareTo(nameAnimUsing) != 0 && blockedAnim == false)
            {
                nameAnimUsing = anim.name;
                skeletonAnimation.state.SetAnimation(0, anim, loop);
            }
        }
        catch (System.Exception e)
        {
            Debug.Log("Error al hacer la animacion. " + anim.name + ". " + e.ToString());
            return false;
        }
        return true;
    }

    public bool changeAnimation(string animName)
    {
        return changeAnimation(findAnimationByName(animName), true);
    }
    public bool changeAnimation(string animName, bool loop)
    {
        return changeAnimation(findAnimationByName(animName), loop);
    }

    //Actions
    public bool addAction(Spine.Animation anim)
    {
       // refreshSetToPose = false;
        if (anim == null)
        { Debug.Log("Error al cambiar la animación, es nula!!!!!!!"); return false; }
        if (changeAnimation(anim, false))//Cambio la animacion
        {
            blockedAnim = true;//Si ha ido bien bloqueo
            return true;
        }
        return false;
    }

    public bool addAction(string anim)
    {
        refreshSetToPose = true;
        return addAction(findAnimationByName(anim));
    }

    public bool addAction(string anim, bool refresSeToPose)
    {
        refreshSetToPose = refresSeToPose;
        return addAction(findAnimationByName(anim));
    }

    //Añadir eventos
    public bool addEvent(string anim)
    {
        return addEvent(findAnimationByName(anim));
    }

    public bool addEvent(Spine.Animation anim)
    {
        try
        {
            skeletonAnimation.state.SetAnimation(1, anim, false);
        }
        catch (System.Exception e)
        {
            Debug.Log("Error al hacer la animacion. " + anim.name + ". " + e.ToString());
            return false;
        }
        return true;
    }

    //Buscar animacion
    public Spine.Animation findAnimationByName(string nameAnim)
    {
        foreach (Spine.Animation _anim in skeletonAnimation.SkeletonDataAsset.GetSkeletonData(false).animations)
            if (_anim.name.CompareTo(nameAnim) == 0)
                return _anim;
        Debug.Log("No se ha encontrado lal animación "+nameAnim);
        return null;
    }

    public void setMovible(bool movible)
    {
        isMovible = movible;
    }

    public void flip()
    {
        skeletonAnimation.skeleton.FlipX = !skeletonAnimation.skeleton.FlipX;
    }

    public bool isLookingLeft() { return skeletonAnimation.skeleton.FlipX;  }

    public void stop()
    {
        if (usingItems)
            changeAnimation(animNames.stopedWithItem);
        else
            changeAnimation(animNames.stoped);
    }

    public void move()
    {
        if (usingItems)
            changeAnimation(animNames.walkWithItem);
        else
            changeAnimation(animNames.walk);
    }

    public AnimationsName getAnimNames() { return animNames; }
    public void setAnimNames(AnimationsName animationNames)  { animNames = animationNames; }
    public void setStopName(string stopName)                 { animNames.stoped = stopName; }
    public void setDropName(string dropName)                 { animNames.drop = dropName;   }
    public void setGetName (string getName)                  { animNames.get = getName;     }
    public void setWalkName(string walkName)                 { animNames.walk = walkName;   }
    public void setObjectAnimNames(string stopName, string dropName, string getName, string walkName)
    {
        animNames.stopedWithItem = stopName;
        animNames.drop = dropName;
        animNames.get = getName;
        animNames.walkWithItem = walkName;
        animNames.stoped = STOPANIM;
        animNames.walk = WALKANIM;
    }
    public void setCollectableAnimNames(CollectableAnimationNames col)
    {
        animNames.stopedWithItem =  col.playerStop;
        animNames.drop =            col.playerDrop;
        animNames.get =             col.playerGetItem;
        animNames.walkWithItem =    col.playerWalk;
    }
    //Inteto evitar este metodo mejor usar get/drop
    public void action()
    {
        if (!usingItems)
        {
            addAction(animNames.get,true);
        }
        else
        {
            addAction(animNames.drop,false);
        }
        usingItems = !usingItems;
    }

    public bool actionGet()
    {
        if (addAction(animNames.get))//TODO Ants tenia puesto el refresh to set pose (añadiendo un true en el segundo parametro)
        {
            usingItems = true;
            return true;
        }
        return false;
    }

    public void actionDrop()
    {
        addAction(animNames.drop, false);
        usingItems = false;
    }

    //Destruir
    void OnDestroy()
    {
        _skeletonAnimation = null;
        movement = null;
    }
}
