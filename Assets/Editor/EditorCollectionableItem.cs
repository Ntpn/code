﻿using UnityEngine;
using UnityEditor;
using Spine.Unity;
using System;

[CustomEditor (typeof(CollectableItem))]
public class EditorCollectionableItem : Editor {

    int selected = 0;
    int selectedIdle = 0;
    int selectedOculto = 1;
    int selectedSoltar = 2;
    string[] options = new string[0];
    string[] anims   = new string[0];
    GameObject player;
    bool showGroup = true;
    bool control = true;



    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        CollectableItem myTarget = target as CollectableItem;
        CollectableItem prefab = (myTarget.getActivators().prefab != null) ? myTarget.getActivators().prefab.GetComponent<CollectableItem>()??null : null;

        ActiveCondition activator = myTarget.getActivators();

        /*int a = Enum.GetValues(typeof(States)).Length; //Sacar el tamaño del enum :D
        Debug.Log(a);*/

        //INIT
        if (control)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            //    string[] options = new string[(int)States.DROP + 1];
            SkeletonAnimation skel = player.transform.GetChild(0).GetComponent<SkeletonAnimation>();

            //int s = skel.AnimationState.Data.SkeletonData.Animations.Count;
            int size = skel.SkeletonDataAsset.GetSkeletonData(false).Animations.Items.Length;

            options = new string[size];
            int _i = 0;
            foreach (Spine.Animation item in skel.SkeletonDataAsset.GetSkeletonData(false).Animations.Items)
            {
                options[_i] = item.Name;
                
                _i++;
            };
            _i = 0;
            anims = new string[myTarget.gameObject.GetComponent<SkeletonAnimation>().SkeletonDataAsset.GetSkeletonData(false).Animations.Items.Length];
            foreach (Spine.Animation item in myTarget.gameObject.GetComponent<SkeletonAnimation>().SkeletonDataAsset.GetSkeletonData(false).Animations.Items)
            {
                anims[_i] = item.Name;
                if (item.Name.CompareTo(myTarget.getHide()) == 0) {
                    selectedOculto = _i; }
                if (item.Name.CompareTo(myTarget.getIdle()) == 0) {
                    selectedIdle   = _i; }
                if (item.Name.CompareTo(myTarget.getDrop()) == 0) {
                    selectedSoltar = _i; }

                _i++;
            }

            control = false;
            
            myTarget.setIdle(anims[selectedIdle]);
            myTarget.setHide(anims[selectedOculto]);
            myTarget.setDrop(anims[selectedSoltar]);
        }
        if (prefab != null)
        {
            for (int i = 0; i < options.Length; i++)
            {
                if (options[i].CompareTo(activator.playerAnim) == 0)
                {
                    selected = i;
                }
            }

            //Mostrar droplist de opciones
            int _selected = EditorGUILayout.Popup("    PlayerAnim", selected, options);
            if (selected != _selected)
            {
                selected = _selected;
                myTarget.setActivators(options[selected]);
                    
                //Guardo la nueva variable
                //myTarget.setIdle(options[selected]);
            }
        }
        else
        {
            myTarget.setActivators("");
        }
        showGroup = EditorGUILayout.Foldout(showGroup, "Lista de animaciones");
        if (showGroup)
        {
            int _selected = EditorGUILayout.Popup("    IDLE", selectedIdle, anims);
            if (selectedIdle != _selected)
            {
                selectedIdle = _selected;
                //myTarget.setActivators(options[selectedIdle]);

                //Guardo la nueva variable
                myTarget.setIdle(anims[selectedIdle]);
            }
            int _selectedO = EditorGUILayout.Popup("    OCULTO", selectedOculto, anims);
            if (selectedOculto != _selectedO)
            {
                selectedOculto = _selectedO;
                //myTarget.setActivators(options[selectedIdle]);

                //Guardo la nueva variable
                myTarget.setHide(anims[selectedOculto]);
            }
            int _selectedS = EditorGUILayout.Popup("    SOLTAR", selectedSoltar, anims);
            if (selectedSoltar != _selectedS)
            {
                selectedSoltar = _selectedS;

                //Guardo la nueva variable
                myTarget.setDrop(anims[selectedSoltar]);
            }
        }
    }

}