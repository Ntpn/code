﻿using UnityEngine;
using UnityEditor;
using Spine.Unity;

[CustomEditor(typeof(Cinematics))]
public class EditorCinematics : Editor
{
    int selectedType;
    int selected = 0;
    string[] types = new string[0];
    string[] options = new string[0];
    //string[] anims = new string[0];
    bool control = true;
    bool showGroup = true;


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Cinematics myTarget = target as Cinematics;

        //INIT
        if (control)
        {
            types = new string[3];
            for (int i = 0; i<= (int)TYPECINEMATIC.WAIT;i++)
            {
                types[i] = ((TYPECINEMATIC)i).ToString();
            }
            control = false;
            reloadOptions(myTarget.getObjects());
        }
        if (myTarget.getObjects().Length != options.Length)
        {
            reloadOptions(myTarget.getObjects());
        }
        showGroup = EditorGUILayout.Foldout(showGroup, "Add Cinematic");
        if (showGroup)
        {
            selectedType = EditorGUILayout.Popup("    Type", selectedType, types);
            if (options.Length > 0)
                selected = EditorGUILayout.Popup("    Objects", selected, options);
        }


    }

    private void reloadOptions(GameObject[] _objects)
    {
        int i = 0;
        options = new string[_objects.Length];
        foreach (GameObject item in _objects)
        {
            options[i] = item.name;
            i++;
        }
    }

}
